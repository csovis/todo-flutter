import 'package:flutter/material.dart';

class RadioButton extends StatelessWidget {
  final bool isChecked;
  final VoidCallback callback;

  RadioButton({@required this.isChecked, @required this.callback});

  @override
  Widget build(BuildContext context) {
    return Row(
      children: <Widget>[
        Container(
          width: 60,
          height: 30,
          child: GestureDetector(
            onTap: callback,
            child: Stack(
              children: <Widget>[
                _RoundedRect(
                  isChecked: isChecked,
                ),
                _RoundBlob(
                  isVisible: !isChecked,
                  color: Color(0xffb5b5b5),
                ),
                Align(
                  alignment: Alignment.topRight,
                  child: _RoundBlob(
                    isVisible: isChecked,
                    color: Color(0xff30EFFD),
                  ),
                ),
              ],
            ),
          ),
        ),
      ],
    );
  }
}

class _RoundedRect extends StatelessWidget {
  final bool isChecked;
  const _RoundedRect({
    Key key,
    this.isChecked,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(left: 3.5, top: 9),
      constraints: BoxConstraints.tightFor(height: 12, width: 50),
      decoration: BoxDecoration(
        color: isChecked ? Color(0xffAEF9FF) : Color(0xffcccccc),
        borderRadius: BorderRadius.circular(20),
        boxShadow: <BoxShadow>[
          BoxShadow(
            spreadRadius: -3,
            blurRadius: 4,
            offset: Offset(0, 1),
          ),
        ],
      ),
    );
  }
}

class _RoundBlob extends StatelessWidget {
  const _RoundBlob({
    Key key,
    @required this.isVisible,
    @required this.color,
  }) : super(key: key);

  final bool isVisible;
  final Color color;

  @override
  Widget build(BuildContext context) {
    return Visibility(
      visible: isVisible,
      child: Container(
        margin: EdgeInsets.only(left: 3.5, top: 4),
        constraints: BoxConstraints.tightFor(height: 22, width: 22),
        decoration: BoxDecoration(
          color: color,
          borderRadius: BorderRadius.circular(20),
          boxShadow: <BoxShadow>[
            BoxShadow(
              spreadRadius: -3,
              blurRadius: 4,
              offset: Offset(0, 1),
            ),
          ],
        ),
      ),
    );
  }
}
