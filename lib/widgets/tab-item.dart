import 'package:flutter/material.dart';
import 'package:todo/bloc-provider.dart';
import 'package:todo/blocs/todos-bloc.dart';
import 'package:todo/blocs/todos-model.dart' as prefix0;

class TabItem extends StatelessWidget {
  final bool isActive;
  final String label;
  final Function onTap;
  final prefix0.Tab index;

  TabItem(
      {this.isActive = false,
      @required this.label,
      @required this.onTap,
      @required this.index});

  Widget isActiveWidget(prefix0.Tab activeIndex) {
    return activeIndex == index
        ? Align(
            alignment: Alignment.bottomCenter,
            child: Container(height: 2, color: Colors.white),
          )
        : SizedBox();
  }

  @override
  Widget build(BuildContext context) {
    return Expanded(
      child: Container(
        height: 33,
        child: Material(
          type: MaterialType.transparency,
          child: InkWell(
            highlightColor: Color(0xFFFF6788),
            splashColor: Color(0xFFFF6788),
            onTap: () => onTap(index),
            child: Stack(
              children: <Widget>[
                Align(
                  alignment: Alignment.center,
                  child: Text(
                    label.toUpperCase(),
                    style: TextStyle(
                      color: Colors.white,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                ),
                isActiveWidget(BlocProvider.of<TodosBloc>(context)
                    ?.todosModel
                    ?.activeTabIndex),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
