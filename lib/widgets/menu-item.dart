import 'package:flutter/material.dart';

class MenuItem extends StatelessWidget {
  final bool isChecked;
  final VoidCallback callback;
  final String title;
  final String subTitle;
  const MenuItem({
    Key key,
    @required this.isChecked,
    @required this.callback,
    @required this.title,
    this.subTitle,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    IconData data;
    if (isChecked) {
      data = Icons.check_circle;
    } else {
      data = Icons.radio_button_unchecked;
    }
    return ListTile(
      leading: GestureDetector(
        onTap: callback,
        child: Icon(
          data,
          size: 36,
          color: Color(0xff30EFFD),
        ),
      ),
      title: Text(title),
      subtitle: Text(subTitle),
    );
  }
}
