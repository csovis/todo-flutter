import 'package:flutter/material.dart';

class FilterOption extends StatelessWidget {
  final String title;
  final bool hasActiveSecondaryFiltering;
  final int bgColor;

  const FilterOption({
    Key key,
    @required this.title,
    this.hasActiveSecondaryFiltering = false,
    this.bgColor = 0xff064A6D,
  }) : super(key: key);

  static const double outerShadowWidth = 4.0;

  @override
  Widget build(BuildContext context) {
    // Non-visible default shadow
    BoxShadow shadow = BoxShadow(spreadRadius: -5);
    Widget circleOrSizedBox;
    // TODO should have another boolean flag, whether this is the currently selected item or not
    // Show red circle if has any filtering, show outter blue ring if its selected
    if (hasActiveSecondaryFiltering) {
      circleOrSizedBox = Align(
        alignment: Alignment.topRight,
        child: Container(
          transform: Matrix4.translationValues(0, -7, 0),
          constraints: BoxConstraints.tightFor(height: 15, width: 15),
          decoration: BoxDecoration(
            color: Color(0xffFF6788),
            borderRadius: BorderRadius.circular(20),
            boxShadow: <BoxShadow>[
              BoxShadow(
                spreadRadius: outerShadowWidth,
                color: Color(0xff52C1FC),
              ),
            ],
          ),
        ),
      );
    } else {
      circleOrSizedBox = SizedBox(
        width: 15,
      );
    }

    if (hasActiveSecondaryFiltering) {
      shadow = BoxShadow(
        spreadRadius: outerShadowWidth,
        color: Color(0xff52C1FC),
      );
    }

    return Container(
      height: 34,
      padding: EdgeInsets.only(left: 15),
      margin: EdgeInsets.only(
          right: outerShadowWidth + 5,
          left: outerShadowWidth + 5,
          top: 8,
          bottom: 5),
      decoration: BoxDecoration(
        color: Color(bgColor),
        borderRadius: BorderRadius.circular(30),
        boxShadow: <BoxShadow>[
          BoxShadow(
            spreadRadius: -2,
            blurRadius: 10,
            offset: Offset(0, 1),
          ),
          shadow,
        ],
      ),
      child: Row(
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          Text(
            title,
            style: TextStyle(
              color: Colors.white,
              fontWeight: FontWeight.bold,
            ),
          ),
          circleOrSizedBox
        ],
      ),
    );
  }
}
