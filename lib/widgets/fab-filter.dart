import 'package:flutter/material.dart';

class FABFilter extends StatelessWidget {
  const FABFilter({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return FloatingActionButton(
      heroTag: 'screen1',
      onPressed: () {},
      tooltip: 'Tooltip text 2',
      backgroundColor: Color(0xff52C1FC),
      child: Container(
        margin: EdgeInsets.only(top: 10, bottom: 8),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: <Widget>[
            Container(
              decoration: BoxDecoration(
                color: Color(0xff30EFFD),
                borderRadius: BorderRadius.all(
                  Radius.circular(20.0),
                ),
              ),
              height: 4.0,
              width: 30.0,
            ),
            Container(
              height: 4.0,
              width: 20.0,
              decoration: BoxDecoration(
                color: Color(0xff30EFFD),
                borderRadius: BorderRadius.all(
                  Radius.circular(20.0),
                ),
              ),
            ),
            Container(
              height: 4.0,
              width: 10.0,
              decoration: BoxDecoration(
                color: Color(0xff30EFFD),
                borderRadius: BorderRadius.all(
                  Radius.circular(20.0),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
