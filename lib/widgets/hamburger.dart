import 'package:flutter/material.dart';

const Color kLineColor = Color(0xFFFF6788);
final BoxDecoration kBoxDecoration = BoxDecoration(
  color: kLineColor,
  borderRadius: BorderRadius.circular(20),
);

class Hamburger extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      width: 56,
      height: 56,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Container(width: 35, height: 5, decoration: kBoxDecoration),
          SizedBox(height: 5),
          Container(width: 35, height: 5, decoration: kBoxDecoration),
          SizedBox(height: 5),
          Container(width: 35, height: 5, decoration: kBoxDecoration),
        ],
      ),
    );
  }
}
