import 'package:flutter/material.dart';

class ThreeStateElevation extends StatelessWidget {
  final Widget child;

  const ThreeStateElevation({Key key, this.child}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Expanded(
      flex: 5,
      child: Stack(
        children: <Widget>[
          Container(
            margin: EdgeInsets.fromLTRB(10, 25, 10, 25),
            decoration: BoxDecoration(
              color: Colors.grey[200],
              borderRadius: BorderRadius.circular(20),
            ),
          ),
          Container(
            margin: EdgeInsets.fromLTRB(25, 15, 25, 15),
            decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.circular(20),
            ),
          ),
          Container(
            margin: EdgeInsets.fromLTRB(30, 25, 30, 25),
            child: child,
          ),
        ],
      ),
    );
  }
}
