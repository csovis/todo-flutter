import 'package:flutter/material.dart';

class DateInputField extends StatelessWidget {
  final kTextLabelStyle =
      TextStyle(color: Colors.grey, fontWeight: FontWeight.w700);

  final String label;
  final String inputValue;
  final TextEditingController inputEditingCtrl;

  DateInputField({this.label, this.inputValue})
      : inputEditingCtrl = TextEditingController(text: inputValue);

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Padding(
          padding: EdgeInsets.only(bottom: 2),
          child: Text(
            label ?? '',
            style: kTextLabelStyle,
          ),
        ),
        Row(
          children: <Widget>[
            Expanded(
              flex: 1,
              child: Text(
                '$inputValue',
                style: TextStyle(color: Colors.grey),
              ),
            ),
            Icon(
              Icons.arrow_drop_down,
              size: 30,
            )
          ],
        ),
        Container(
          margin: EdgeInsets.only(top: 7),
          decoration: BoxDecoration(
            gradient: LinearGradient(
              stops: [0, 1],
              colors: [Color(0xFFE5E5E5), Color(0xFFFFFFFF)],
              begin: Alignment.topCenter,
              end: Alignment.bottomCenter,
            ),
          ),
          height: 4,
        )
      ],
    );
  }
}
