import 'package:flutter/material.dart';

class FilterIcon extends StatelessWidget {
  final Color bgLineColor;

  FilterIcon({@required this.bgLineColor});

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 36,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: <Widget>[
          Container(
            decoration: BoxDecoration(
              color: bgLineColor,
              borderRadius: BorderRadius.all(
                Radius.circular(20.0),
              ),
            ),
            height: 4.0,
            width: 30.0,
          ),
          Container(
            height: 4.0,
            width: 20.0,
            decoration: BoxDecoration(
              color: bgLineColor,
              borderRadius: BorderRadius.all(
                Radius.circular(20.0),
              ),
            ),
          ),
          Container(
            height: 4.0,
            width: 10.0,
            decoration: BoxDecoration(
              color: bgLineColor,
              borderRadius: BorderRadius.all(
                Radius.circular(20.0),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
