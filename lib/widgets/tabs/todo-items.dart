import 'package:flutter/material.dart';
import 'package:todo/bloc-provider.dart';
import 'package:todo/blocs/todos-bloc.dart';
import 'package:todo/widgets/menu-item.dart';

class TodoItemsTab extends StatelessWidget {
  final model = (context) => BlocProvider.of<TodosBloc>(context);

  Iterable<MenuItem> getChildrens(context) sync* {
    var items = model(context).todosModel.items;
    for (var i = 0; i < items.length; i++) {
      var item = items[i];

      yield MenuItem(
        title: item.description,
        subTitle: item.date.toIso8601String(),
        callback: () {
          model(context).toggleItemStatus(item.id);
        },
        isChecked: item.isFinished,
      );
    }
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      child: ListView(
        children: getChildrens(context).toList(),
      ),
    );
  }
}
