import 'dart:math';

import 'package:flutter/material.dart';
import 'package:todo/models/caregory.dart';

import 'category-option.dart';

class CategoryInputField extends StatefulWidget {
  final List<Category> inputOptions;

  final Function itemsChanged;
  final String label;

  CategoryInputField({this.label, this.inputOptions, this.itemsChanged});

  @override
  _CategoryInputFieldState createState() => _CategoryInputFieldState();
}

class _CategoryInputFieldState extends State<CategoryInputField> {
  List<Category> inputOptions = List<Category>();

  void deletedItem(int id) {
    setState(() {
      inputOptions.removeWhere((x) => x.id == id);
      widget.itemsChanged(inputOptions);
    });
  }

  void addedItem(Category item) {
    item.id = Random().nextInt(90000) + 10000;
    setState(() {
      inputOptions.add(item);
      widget.itemsChanged(inputOptions);
    });
  }

  void modifyItem(Category item) {
    setState(() {
      var cat = inputOptions.where((x) => x.id == item.id).single;
      if (cat != null) {
        cat.title = item.title;
      }
      widget.itemsChanged(inputOptions);
    });
  }

  Iterable<Widget> getOptions() sync* {
    for (var i = 0; i < inputOptions.length; i++) {
      final item = inputOptions[i];
      yield CategoryOption(
        key: Key(item.id.toString()),
        id: item.id,
        title: item.title,
        onDelete: deletedItem,
        onItemChanged: modifyItem,
        limitWidth: true,
      );
    }
    yield GestureDetector(
      onTap: () => addedItem(Category(0, '')),
      child: CategoryOption(
        id: -1,
        title: "Add new",
        onDelete: deletedItem,
        limitWidth: true,
        backgroundColor: Colors.green,
        inputDisabled: true,
      ),
    );
  }

  @override
  void initState() {
    if (widget.inputOptions != null && widget.inputOptions.length > 0) {
      setState(() {
        inputOptions = widget.inputOptions;
      });
    }
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Container(
            margin: EdgeInsets.symmetric(vertical: 4),
            child: Text(
              widget.label,
              style: TextStyle(fontWeight: FontWeight.w700, color: Colors.grey),
            ),
          ),
          Container(
            child: Wrap(
              children: getOptions().toList(),
            ),
          ),
          Container(
            margin: EdgeInsets.only(top: 7),
            decoration: BoxDecoration(
              gradient: LinearGradient(
                stops: [0, 1],
                colors: [Color(0xFFE5E5E5), Color(0xFFFFFFFF)],
                begin: Alignment.topCenter,
                end: Alignment.bottomCenter,
              ),
            ),
            height: 4,
          )
        ],
      ),
    );
  }
}
