import 'package:flutter/material.dart';

class OneStateElevation extends StatelessWidget {
  final Widget child;
  final Function onDismiss;

  const OneStateElevation({Key key, this.child, this.onDismiss})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Expanded(
      child: Stack(
        children: <Widget>[
          Container(
            margin: EdgeInsets.fromLTRB(10, 45, 10, 25),
            decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.circular(20),
            ),
          ),
          Container(
            margin: EdgeInsets.fromLTRB(30, 45, 30, 25),
            child: child,
          ),
          Padding(
            padding: EdgeInsets.only(right: 16, top: 16),
            child: Align(
              alignment: Alignment.topRight,
              child: FloatingActionButton(
                heroTag: 'screen5423',
                onPressed: () {
                  this.onDismiss(false);
                },
                tooltip: 'Cancel adding new todo item',
                child: Icon(
                  Icons.add,
                  color: Colors.white,
                  size: 45,
                ),
                mini: false,
                backgroundColor: Color(0xffFF6788),
              ),
            ),
          ),
          Container(
            padding: EdgeInsets.only(right: 16),
            margin: EdgeInsets.only(bottom: 0, top: 15),
            child: Align(
              alignment: Alignment.bottomCenter,
              child: FloatingActionButton(
                heroTag: 'screen23654',
                onPressed: () {
                  this.onDismiss(true);
                },
                tooltip: 'Save todo item',
                child: Icon(
                  Icons.check,
                  color: Colors.white,
                  size: 45,
                ),
                mini: false,
                backgroundColor: Color(0xff30EFFD),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
