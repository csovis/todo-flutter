import 'package:flutter/material.dart';
import 'package:todo/models/caregory.dart';

class CategoryOption extends StatefulWidget {
  final int id;
  final String title;

  final Function onDelete;
  final Function onItemChanged;

  final bool limitWidth;
  final Color backgroundColor;
  final bool inputDisabled;

  const CategoryOption({
    Key key,
    @required this.id,
    @required this.title,
    this.limitWidth = false,
    this.onDelete,
    this.onItemChanged,
    this.backgroundColor,
    this.inputDisabled = false,
  }) : super(key: key);

  @override
  _CategoryOptionState createState() => _CategoryOptionState();
}

class _CategoryOptionState extends State<CategoryOption> {
  final textController = TextEditingController();

  void textControllerListener() {
    widget.onItemChanged(Category(widget.id, textController.text));
    print('text changed: ${textController.text} id: ${widget.id}');
  }

  @override
  void initState() {
    textController.text = widget.title;

    textController.addListener(textControllerListener);

    focusNode.addListener(() {
      if (!focusNode.hasFocus && widget.title.trim().isEmpty) {
        widget.onDelete(widget.id);
      }
    });
    super.initState();
  }

  final screenWidth = (context) => MediaQuery.of(context).size;
  final focusNode = FocusNode();
  bool firstRun = true;

  @override
  void dispose() {
    textController.removeListener(textControllerListener);
    focusNode.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    if (firstRun && widget.title.length == 0) {
      FocusScope.of(context).requestFocus(focusNode);
    }
    return Container(
      width: screenWidth(context).width <= 320
          ? screenWidth(context).width * 0.39
          : 120,
      decoration: BoxDecoration(
        color: widget.backgroundColor == null
            ? Color(0xff52C1FC)
            : widget.backgroundColor,
        borderRadius: BorderRadius.circular(30),
        boxShadow: <BoxShadow>[
          BoxShadow(
            spreadRadius: -2,
            blurRadius: 4,
            offset: Offset(0, 1),
          ),
        ],
      ),
      padding: EdgeInsets.only(left: 15),
      child: Row(
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          Flexible(
            child: TextField(
              focusNode: focusNode,
              enabled: !widget.inputDisabled,
              controller: textController,
              style: TextStyle(color: Colors.white),
            ),
          ),
          // Text(
          //   title,
          //   style: TextStyle(color: Colors.white),
          // ),
          Container(
            margin: EdgeInsets.all(6).copyWith(right: 10),
            constraints: BoxConstraints.tightFor(height: 23, width: 23),
            decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.circular(20),
            ),
            child: GestureDetector(
              onTap: () => widget.onDelete(widget.id),
              child: Icon(
                Icons.close,
                color: Color(0xff52C1FC),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
