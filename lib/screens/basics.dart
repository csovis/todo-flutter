import 'dart:math';

import 'package:flutter/material.dart';
import 'package:todo/models/caregory.dart';
import 'package:todo/widgets/category-input-field.dart';
import 'package:todo/widgets/category-option.dart';
import 'package:todo/widgets/fab-filter.dart';
import 'package:todo/widgets/filter-icon.dart';
import 'package:todo/widgets/filter-option.dart';
import 'package:todo/widgets/hamburger.dart';
import 'package:todo/widgets/menu-item.dart';
import 'package:todo/widgets/radio-button.dart';
import 'package:todo/widgets/tab-item.dart';

class Basics extends StatefulWidget {
  @override
  _BasicsState createState() => _BasicsState();
}

class _BasicsState extends State<Basics> {
  bool isChecked = false;
  bool menuItemChecked = false;

  List<Category> inputOptions = List<Category>()
    ..add(Category(1, 'Purchasee qwe'))
    ..add(Category(2, 'Other cat'))
    ..add(Category(3, 'Other cat'))
    ..add(Category(4, 'Other cat'))
    ..add(Category(5, 'Other cat'));

  void deletedItem(int id) {
    setState(() {
      inputOptions.removeWhere((x) => x.id == id);
    });
  }

  void addedItem(Category item) {
    item.id = Random().nextInt(90000) + 10000;
    setState(() {
      inputOptions.add(item);
    });
  }

  void modifyItem(Category item) {
    setState(() {
      var cat = inputOptions.where((x) => x.id == item.id).single;
      if (cat != null) {
        cat.title = item.title;
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    //inputOptions.forEach((item) => print('${item.title}, id: ${item.id}'));
    return Scaffold(
      // appBar: AppBar(
      //   title: Text('Some screen'),
      // ),
      body: SafeArea(
        child: Container(
          child: ListView(
            //crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              FABFilter(),
              FloatingActionButton(
                heroTag: 'screen2',
                onPressed: () {
                  Navigator.pushNamed(context, '/animation');
                },
                tooltip: 'Tooltip text',
                child: Icon(
                  Icons.add,
                  color: Colors.white,
                  size: 45,
                ),
                mini: false,
                backgroundColor: Color(0xff30EFFD),
              ),
              FloatingActionButton(
                heroTag: 'screen3',
                onPressed: () {},
                tooltip: 'Tooltip text',
                child: Icon(
                  Icons.close,
                  color: Colors.white,
                  size: 45,
                ),
                mini: false,
                backgroundColor: Color(0xffFF6788),
              ),
              Wrap(
                children: <Widget>[
                  CategoryOption(
                    title: 'Purchase qwe',
                  ),
                  CategoryOption(
                    title: 'Sm',
                  ),
                  CategoryOption(
                    title: 'Sm',
                  ),
                  CategoryOption(
                    title: 'Sma sd asd as dasdas',
                  )
                ],
              ),
              RadioButton(
                isChecked: isChecked,
                callback: () => setState(() => isChecked = !isChecked),
              ),
              MenuItem(
                title: 'Some main title',
                subTitle: 'Some secondary text subtitle',
                callback: () {
                  setState(() {
                    menuItemChecked = !menuItemChecked;
                  });
                },
                isChecked: menuItemChecked,
              ),
              Row(
                children: <Widget>[
                  FilterOption(
                    title: 'Purchase',
                    hasActiveSecondaryFiltering: false,
                  ),
                  FilterOption(
                    title: 'True eeeee',
                    hasActiveSecondaryFiltering: true,
                  ),
                  FilterOption(
                    title: 'True',
                    bgColor: 0xffFF6788,
                  ),
                ],
              ),
              Hamburger(),
              FilterIcon(
                bgLineColor: Color(0x8000FF00),
              ),
              Container(
                color: Colors.green,
                child: Row(
                  mainAxisSize: MainAxisSize.max,
                  children: <Widget>[
                    TabItem(
                      label: 'Yesterday',
                    ),
                    TabItem(label: 'Today', isActive: true),
                    TabItem(label: 'Tomorrow'),
                  ],
                ),
              ),
              Column(
                children: <Widget>[
                  Container(
                    decoration: BoxDecoration(color: Color(0xB3064A6D)),
                    padding: EdgeInsets.only(bottom: 8, top: 5),
                    child: Row(
                      children: <Widget>[
                        FilterOption(
                          title: 'By category',
                          hasActiveSecondaryFiltering: true,
                        ),
                        FilterOption(
                          title: 'By label',
                          hasActiveSecondaryFiltering: false,
                        ),
                      ],
                    ),
                  ),
                  Container(
                    decoration: BoxDecoration(color: Color(0xFF375F76)),
                    padding: EdgeInsets.only(bottom: 8, top: 0),
                    child: Row(
                      children: <Widget>[
                        Flexible(
                          child: Wrap(
                            direction: Axis.horizontal,
                            children: <Widget>[
                              FilterOption(
                                title: 'some shit',
                                hasActiveSecondaryFiltering: false,
                              ),
                              FilterOption(
                                title: 'some shit 2',
                                hasActiveSecondaryFiltering: false,
                              ),
                              FilterOption(
                                title: 'some shit qwe 3',
                                hasActiveSecondaryFiltering: false,
                              ),
                              FilterOption(
                                title: 'some shit',
                                hasActiveSecondaryFiltering: false,
                              ),
                              FilterOption(
                                title: 'True',
                                bgColor: 0xffFF6788,
                              ),
                            ],
                          ),
                        ),
                      ],
                    ),
                  ),
                  Container(
                    decoration: BoxDecoration(color: Color(0xFFFF6788)),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Container(
                          margin: EdgeInsets.only(top: 15, bottom: 15),
                          child: FilterIcon(
                            bgLineColor: Color(0x80FFFFFF),
                          ),
                        ),
                        SizedBox(
                          width: 80,
                        ),
                        Icon(
                          Icons.close,
                          color: Color(0x80FFFFFF),
                          size: 45,
                        ),
                      ],
                    ),
                  ),
                ],
              ),
              CategoryInputField(
                label: 'Test title',
                inputOptions: inputOptions,
              ),
            ],
          ),
        ),
      ),
    );
  }
}
