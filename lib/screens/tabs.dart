import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:todo/bloc-provider.dart';
import 'package:todo/blocs/todos-bloc.dart';
import 'package:todo/blocs/todos-model.dart' as prefix0;
import 'package:todo/widgets/fab-filter.dart';
import 'package:todo/widgets/hamburger.dart';
import 'package:todo/widgets/tab-item.dart';
import 'package:todo/widgets/tabs/todo-items.dart';
import 'package:todo/widgets/three-stage-elevation.dart';

const kTabsLabeltextStyle = TextStyle(
  color: Color(0xFFFFFFFF),
  fontWeight: FontWeight.bold,
  fontSize: 48,
);

class TabsScreen extends StatefulWidget {
  @override
  _TabsScreenState createState() => _TabsScreenState();
}

class _TabsScreenState extends State<TabsScreen> {
  void tabChange(prefix0.Tab index, context) {
    BlocProvider.of<TodosBloc>(context).changeIndex(index);
  }

  @override
  void initState() {
    tabChange(prefix0.Tab.today, this.context);
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    DateTime today = new DateTime.now();
    String dateSlug =
        "${today.year.toString()}-${today.month.toString().padLeft(2, '0')}-${today.day.toString().padLeft(2, '0')}";

    return Scaffold(
      floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
      floatingActionButton: Container(
        padding: EdgeInsets.fromLTRB(15, 0, 15, 25),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            FloatingActionButton(
              heroTag: 'screen2',
              onPressed: () async {
                var results = await Navigator.pushNamed(context, '/new');
                print("new navigation results: $results");
                if (results != null) {
                  // Save(add or modifyform item
                }
              },
              tooltip: 'Tooltip text',
              child: Icon(
                Icons.add,
                color: Colors.white,
                size: 45,
              ),
              mini: false,
              backgroundColor: Color(0xff30EFFD),
            ),
            FABFilter(),
          ],
        ),
      ),
      body: StreamBuilder(
        stream: BlocProvider.of<TodosBloc>(context).stream,
        builder: (buildContext, snapshot) {
          if (snapshot.hasData) {
            return Container(
              decoration: BoxDecoration(
                gradient: LinearGradient(
                  stops: [0, 1],
                  colors: [Color(0xFF30EFFD), Color(0xFF52C1FC)],
                  begin: Alignment.topCenter,
                  end: Alignment.bottomCenter,
                ),
              ),
              child: SafeArea(
                child: LayoutBuilder(
                  builder: (BuildContext context,
                      BoxConstraints viewportConstraints) {
                    return Column(
                      children: <Widget>[
                        Expanded(
                          flex: 1,
                          child: Row(
                            children: <Widget>[
                              Hamburger(),
                              Container(
                                margin: EdgeInsets.only(left: 5),
                                child: Text(
                                  'To-do List',
                                  style: kTabsLabeltextStyle,
                                ),
                              ),
                            ],
                          ),
                        ),
                        Container(
                          alignment: Alignment.centerLeft,
                          padding:
                              EdgeInsets.all(20).copyWith(top: 0, bottom: 15),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.start,
                            children: <Widget>[
                              Icon(
                                Icons.calendar_today,
                                color: Colors.white,
                                size: 15,
                              ),
                              SizedBox(width: 5),
                              Text(
                                '$dateSlug',
                                style: TextStyle(
                                    color: Colors.white,
                                    fontSize: 11,
                                    fontWeight: FontWeight.bold),
                              )
                            ],
                          ),
                        ),
                        Container(
                          alignment: Alignment.bottomCenter,
                          child: Row(
                            mainAxisSize: MainAxisSize.max,
                            children: <Widget>[
                              TabItem(
                                index: prefix0.Tab.yesterday,
                                onTap: (index) =>
                                    tabChange(prefix0.Tab.yesterday, context),
                                label: 'Yesterday',
                              ),
                              TabItem(
                                index: prefix0.Tab.today,
                                onTap: (index) =>
                                    tabChange(prefix0.Tab.today, context),
                                label: 'Today',
                              ),
                              TabItem(
                                index: prefix0.Tab.tomorrow,
                                onTap: (index) =>
                                    tabChange(prefix0.Tab.tomorrow, context),
                                label: 'Tomorrow',
                              ),
                            ],
                          ),
                        ),
                        Container(
                          height: 4,
                          decoration: BoxDecoration(
                            gradient: LinearGradient(
                                begin: Alignment.topCenter,
                                end: Alignment.bottomCenter,
                                stops: [0, 1],
                                colors: [Color(0xFF52C1FC), Color(0x00ffffff)]),
                          ),
                        ),
                        ThreeStateElevation(
                          child: TodoItemsTab(),
                        ),
                      ],
                    );
                  },
                ),
              ),
            );
          } else {
            return CircularProgressIndicator();
          }
        },
      ),
    );
  }
}
