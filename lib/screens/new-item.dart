import 'package:flutter/material.dart';
import 'package:todo/models/Item.dart';
import 'package:todo/screens/new-item/todo-form.dart';
import 'package:todo/widgets/one-stage-elevation.dart';

class NewItem extends StatefulWidget {
  @override
  _NewItemState createState() => _NewItemState();
}

class _NewItemState extends State<NewItem> {
  TodoItem dataScope;
  void getItemRef(TodoItem data) {
    dataScope = data;
  }

  void finish(bool finished) {
    print('todo form finished with state: $finished');
    if (finished) {
      Navigator.pop(context, dataScope);
    } else {
      Navigator.pop(context, null);
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        decoration: BoxDecoration(
          gradient: LinearGradient(
            stops: [0, 1],
            colors: [Color(0xFF30EFFD), Color(0xFF52C1FC)],
            begin: Alignment.topCenter,
            end: Alignment.bottomCenter,
          ),
        ),
        child: SafeArea(
          child: LayoutBuilder(
            builder:
                (BuildContext context, BoxConstraints viewportConstraints) {
              return SingleChildScrollView(
                child: ConstrainedBox(
                  constraints:
                      BoxConstraints(minHeight: viewportConstraints.maxHeight),
                  child: IntrinsicHeight(
                    child: Column(
                      children: <Widget>[
                        OneStateElevation(
                          onDismiss: finish,
                          child: TodoForm(
                            getItemRef: getItemRef,
                          ),
                        ),
                        SizedBox(
                          height: 25,
                        )
                      ],
                    ),
                  ),
                ),
              );
            },
          ),
        ),
      ),
    );
  }
}
