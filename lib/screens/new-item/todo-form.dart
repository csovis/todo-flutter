import 'package:flutter/material.dart';
import 'package:todo/models/Item.dart';
import 'package:todo/models/caregory.dart';
import 'package:todo/widgets/category-input-field.dart';
import 'package:todo/widgets/date-input-field.dart';
import 'package:todo/widgets/radio-button.dart';

class TodoForm extends StatefulWidget {
  final Function getItemRef;

  const TodoForm({Key key, this.getItemRef}) : super(key: key);

  @override
  _TodoFormState createState() => _TodoFormState();
}

class _TodoFormState extends State<TodoForm> {
  TodoItem item = TodoItem()
    ..remind = false
    ..categories = (List<Category>()
      ..add(Category(1, 'Category'))
      ..add(Category(5, 'Other cat')))
    ..labels = (List<Category>()
      ..add(Category(1, 'Label qwe'))
      ..add(Category(5, 'Other label')));

  final screenSize = (context) => MediaQuery.of(context).size;
  var kTextLabelStyle =
      TextStyle(color: Colors.grey, fontWeight: FontWeight.w700);

  final descriptionController = TextEditingController();

  final List<String> months = [
    'Jan',
    'Febr',
    'March',
    'Apr',
    'Maj',
    'June',
    'July',
    'Sept',
    'Okt',
    'Nov',
    'Dec'
  ];

  String _formatDate() {
    var date = item.date;
    return '${date.year}. ${months[date.month]}. ${date.day}.';
  }

  String _formatTime() {
    var time = item.time;
    return '${time.hour}:${time.minute}';
  }

  @override
  void initState() {
    // Pass the reference to parent, so its acccessible from there
    widget.getItemRef(item);

    descriptionController
        .addListener(() => item.description = descriptionController.text);
    super.initState();
  }

  @override
  void dispose() {
    descriptionController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    var useBiggerLayout = screenSize(context).width > 480;
    Container leftSideDateInput = Container(
      width:
          useBiggerLayout ? screenSize(context).width * 0.4 : double.infinity,
      child: GestureDetector(
        onTap: () async {
          DateTime selectedDate = await showDatePicker(
              context: context,
              initialDate: DateTime.now(),
              firstDate: DateTime(2019),
              lastDate: DateTime(2040));
          if (selectedDate != null) {
            setState(() {
              item.date = selectedDate;
            });
          }
        },
        child: DateInputField(
          label: 'Date',
          inputValue: item.date == null ? 'Select date' : _formatDate(),
        ),
      ),
    );
    Container rightSideDateInput = Container(
      width:
          useBiggerLayout ? screenSize(context).width * 0.4 : double.infinity,
      child: GestureDetector(
        onTap: () async {
          TimeOfDay selectedTime = await showTimePicker(
            initialTime: TimeOfDay.now(),
            context: context,
          );
          if (selectedTime != null) {
            setState(() {
              item.time = selectedTime;
            });
          }
        },
        child: DateInputField(
          label: 'Time',
          inputValue: item.time == null ? 'Select time' : _formatTime(),
        ),
      ),
    );
    Widget datesInput;
    if (useBiggerLayout) {
      datesInput = Row(
        children: <Widget>[
          leftSideDateInput,
          SizedBox(
            width: 5,
          ),
          rightSideDateInput,
        ],
      );
    } else {
      datesInput = Column(
        children: <Widget>[
          leftSideDateInput,
          rightSideDateInput,
        ],
      );
    }

    // Form widget causing StreamBuilder to rerender :(
    return Container(
      child: Column(
        children: <Widget>[
          TextFormField(
            controller: descriptionController,
            decoration: InputDecoration(
              hintText: 'Description',
              labelText: 'Note',
              labelStyle: kTextLabelStyle,
            ),
            maxLines: 2,
            validator: (value) {
              if (value.isEmpty) {
                return 'Enter some text';
              }
              return null;
            },
          ),
          SizedBox(
            height: 20,
          ),
          CategoryInputField(
            label: 'Category',
            inputOptions: item.categories,
            itemsChanged: (List<Category> items) =>
                print('new items count: ${items.length}'),
          ),
          SizedBox(
            height: 20,
          ),
          CategoryInputField(
            label: 'Labels',
            inputOptions: item.labels,
            itemsChanged: (List<Category> items) =>
                print('new items count: ${items.length}'),
          ),
          SizedBox(
            height: 20,
          ),
          datesInput,
          Padding(
            padding: EdgeInsets.only(top: 6, bottom: 36),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Text(
                  'Remind me',
                  style: TextStyle(
                      color: Color(0xFF52C1FC),
                      fontWeight: FontWeight.w800,
                      fontSize: 16),
                ),
                RadioButton(
                  isChecked: item.remind,
                  callback: () => setState(() => item.remind = !item.remind),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
