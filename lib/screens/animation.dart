import 'package:flutter/material.dart';

class AnimationScreen extends StatefulWidget {
  @override
  _AnimationScreenState createState() => _AnimationScreenState();
}

class _AnimationScreenState extends State<AnimationScreen> {
  //with SingleTickerProviderStateMixin {
  //AnimationController controller;

  // @override
  // void initState() {
  //   super.initState();
  //   // controller = AnimationController(
  //   //   vsync: this,
  //   //   duration: Duration(seconds: 2),
  //   // );
  // }

  // @override
  // void dispose() {
  //   //controller.dispose();
  //   super.dispose();
  // }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      // appBar: AppBar(
      //   title: Text('Animation screen'),
      // ),
      body: SafeArea(
        child: Center(
          child: Column(
            children: <Widget>[
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  RaisedButton(
                    child: Text('Start'),
                    onPressed: () {
                      print('Mat pressed 1');
                    },
                  ),
                  RaisedButton(
                    child: Text('2nd'),
                    onPressed: () {
                      print('Mat pressed 2');
                    },
                  ),
                ],
              ),
              Expanded(
                child: Container(
                  margin: EdgeInsets.all(25).copyWith(right: 35, left: 10),
                  decoration: BoxDecoration(
                    color: Colors.blue[200],
                    borderRadius: BorderRadius.circular(20),
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
