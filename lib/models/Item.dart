import 'package:flutter/material.dart';
import 'package:todo/models/caregory.dart';

class TodoItem {
  int id;
  String description;
  TimeOfDay time;
  DateTime date;
  bool remind;
  double remindBeforeMinutes;

  bool isFinished;
  DateTime finishedOn;

  List<Category> categories;
  List<Category> labels;
}
