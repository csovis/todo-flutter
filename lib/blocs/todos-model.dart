import 'package:todo/models/Item.dart';

class TodosModel {
  Tab activeTabIndex = Tab.today;

  List<TodoItem> items = [];
}

enum Tab { yesterday, today, tomorrow }
