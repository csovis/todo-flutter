import 'package:todo/bloc-provider.dart';
import 'package:rxdart/rxdart.dart';
import 'package:todo/blocs/todos-model.dart';
import 'package:todo/models/Item.dart';

class TodosBloc extends BlocBase {
  BehaviorSubject<TodosModel> _todosModel =
      BehaviorSubject<TodosModel>.seeded(TodosModel());

  Observable<TodosModel> get stream => _todosModel.stream;
  TodosModel get todosModel => _todosModel.value;

  // Private members
  List<TodoItem> _everyItems = [
    TodoItem()
      ..description = "Some description"
      ..id = 1
      ..date = DateTime.now()
      ..isFinished = true
  ];

  void changeIndex(Tab newIndex) {
    todosModel.activeTabIndex = newIndex;

    _getItemsByActiveTab();
  }

  void addItem(TodoItem item) {
    _everyItems.add(item);

    _getItemsByActiveTab();
  }

  void toggleItemStatus(int id) {
    var item = _everyItems.singleWhere((x) => x.id == id);
    item.isFinished = !item.isFinished;

    _modifyItem(id, item);
  }

  void _modifyItem(int id, TodoItem modifiedItem) {
    List<TodoItem> newArray = List<TodoItem>();
    for (var i = 0; i < _everyItems.length; i++) {
      var currentItem = _everyItems[i];

      if (id != currentItem.id) {
        newArray.add(currentItem);
      } else {
        newArray.add(modifiedItem);
      }
    }
    _everyItems = newArray;

    _getItemsByActiveTab();
  }

  // TODO Write tests
  void _getItemsByActiveTab() {
    var dtNow = DateTime.now();
    var todayZero = DateTime.now().subtract(
      Duration(hours: dtNow.hour, minutes: dtNow.minute, seconds: dtNow.second),
    );
    var todayEnd = todayZero.add(
      Duration(hours: 24, minutes: 60, seconds: 60),
    );

    List<TodoItem> items;
    switch (todosModel.activeTabIndex) {
      case Tab.today:
        items = _everyItems
            .where(
              (x) => x.date.isBefore(todayEnd) && x.date.isAfter(todayZero),
            )
            .toList();
        break;
      case Tab.tomorrow:
        items = _everyItems
            .where(
              (x) =>
                  x.date.isBefore(todayEnd.add(Duration(days: 1))) &&
                  x.date.isAfter(todayEnd.add(Duration(seconds: 1))),
            )
            .toList();
        break;
      case Tab.yesterday:
        items = _everyItems
            .where(
              (x) =>
                  x.date.isBefore(todayZero.subtract(Duration(seconds: 1))) &&
                  x.date.isAfter(todayZero.subtract(Duration(days: 1))),
            )
            .toList();
        break;
      default:
    }

    todosModel.items = items;
    _todosModel.sink.add(todosModel);
  }

  void dispose() async {
    await _todosModel.close();
  }
}
