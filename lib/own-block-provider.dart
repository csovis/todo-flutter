import 'package:flutter/material.dart';

class OwnBlocProvider<T> extends StatefulWidget {
  final Widget child;
  final T bloc;

  OwnBlocProvider({
    Key key,
    this.child,
    this.bloc,
  });

  @override
  _OwnBlocProviderState<T> createState() => _OwnBlocProviderState<T>();
}

class _OwnBlocProviderState<T> extends State<OwnBlocProvider> {
  final Widget child;
  final T bloc;

  _OwnBlocProviderState({
    Key key,
    this.child,
    this.bloc,
  });

  @override
  Widget build(BuildContext context) {
    return Container();
  }
}

class _ownBlocProvider<T> extends InheritedWidget {
  const _ownBlocProvider({
    Key key,
    @required this.value,
    @required Widget child,
  })  : assert(value != null),
        assert(child != null),
        super(key: key, child: child);
  final T value;
  static _ownBlocProvider of(BuildContext context) {
    return context.inheritFromWidgetOfExactType(_ownBlocProvider);
  }

  @override
  bool updateShouldNotify(_ownBlocProvider old) => true; //value != old.value;
}
