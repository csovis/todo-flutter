import 'package:flutter/material.dart';
import 'package:todo/bloc-provider.dart';
import 'package:todo/blocs/todos-bloc.dart';
import 'package:todo/screens/animation.dart';
import 'package:todo/screens/basics.dart';
import 'package:todo/screens/new-item.dart';
import 'package:todo/screens/tabs.dart';

void main() => runApp(MyHomePage());

class MyHomePage extends StatefulWidget {
  MyHomePage();

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  final bloc = TodosBloc();

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(initialRoute: '/tabs', routes: {
      '/': (context) => Basics(),
      '/animation': (context) => AnimationScreen(),
      '/tabs': (context) => BlocProvider<TodosBloc>(
            bloc: bloc,
            child: TabsScreen(),
          ),
      '/new': (context) => NewItem()
    });
  }
}
